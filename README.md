# Bookshelf API

Aplikasi backend sederhana yang menyediakan API untuk menambah,mengubah, menampilkan dan menghapus data Post. Aplikasi merupakan implementasi untuk tutorial Javascript Mistery untuk Video [Fullstack MERN Project](https://www.youtube.com/watch?v=ngc9gnGgUdA&list=PL6QREj8te1P7VSwhrMf3D3Xt4V6_SRkhu&index=1) 

## Technology

- Node JS 16.19.0
- npm 8.19.3
- mongoDB

### Installation

```bash
$ npm install
```

## Running the app

```bash


$ cp .env.example .env #  Copy .env-example and modify the .env file
$ npm run start   # make sure you have installed mongo

# production mode
$ npm run start:prod

```

## API Endpoint

## Post 

#### Get all Post
Get all data from Post.

```http
GET /posts/
```

#### Add Post
Create posts data to the system. 
```http
POST /posts/
```
#### Parameter
itle:String,
    message:String,
    creator: String,
    tags: [String],
    selectedFile: String,
    likeCount:{
        type: Number,
        default:0
    },
    createdAt:{
        type:Date,
        default: new Date()
    }
| Key     |  Description                       |
| :-------- | :-------------------------------- |
| `title` |  title of the post |
| `message` |   message of the post publisher |
| `creator` | creator of the post |
| `tags` | List of tag of the post |
| `selectedFile` | base64 string of image |
| `likeCount` | number indicates how many like |

#### PATCH Post 
Update post data with specific id. The request body has same structure with the POST method.
```http
PATCH /posts/${id}
```

#### DELETE Post 
Delete post data with specific id.
```http
DELETE /posts/${id}
```

#### PATCH Like Post 
Increment like post data with specific id.
```http
PATCH /posts/${id}/likePost
```
