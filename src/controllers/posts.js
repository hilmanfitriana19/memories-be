import mongoose from "mongoose";
import PostMessage from "../models/postMessage.js";

export const getPosts = async (req,res) =>{
    try{
        const result = await PostMessage.find();
        return res.json(result)
    }catch(error){
        return res.status(500).json({'message':error.message});
    }
}

export const createPost = async (req,res) =>{
    
    const post = req.body;
    const newPost = new PostMessage(post);

    try{
        await newPost.save();
        return res.status(201).json(newPost);
    }catch(error){ 
        res.status(409).json({message:error.message})
    }
}

export const updatePost = async (req,res) =>{
    
    const { id:_id } = req.params;
    const post = req.body;
    
    if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No Post with that id');

    const updated = await PostMessage.findByIdAndUpdate(_id,{...post,_id},{new:true});
    res.json(updated);
}

export const deletePost = async(req,res) => {
    const {id} = req.params;

    if(!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('No Post with that id');

    await PostMessage.findByIdAndRemove(id);
    res.json({message:'Post deleted Successsfully'})

}

export const likePost = async(req,res) => {
    const {id} = req.params

    if(!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('No Post with that id');

    const post = await PostMessage.findById(id);
    const updatePost = await PostMessage.findByIdAndUpdate(id,{likeCount:post.likeCount+1},{new:true})
    res.json(updatePost)

}