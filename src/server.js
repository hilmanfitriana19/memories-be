import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';
import postRoutes from './routes/posts.js';

dotenv.config()

const app = express()

// limit max upload file 
app.use(express.json({'limit':'30mb','extended':true}))
app.use(express.urlencoded({'limit':'30mb','extended':true}))

app.use(cors())

app.use('/posts',postRoutes)

app.get('/',(req,res)=>{
    res.send('Hello to Memories API')
})

const DB_URL = process.env.DB_URL
mongoose.connect(DB_URL).then((result)=>{
    console.log('success')
}).catch((error)=>{
    console.log(error);
    console.log('error')
    process.exit;
});

const port = process.env.PORT || 5000


app.listen(port,()=>{
    console.log(`Run Application in port ${port}`)
})